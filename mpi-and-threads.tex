\documentclass[a4paper,american]{article}

\usepackage{booktabs}
\usepackage[utf8]{inputenc}
\usepackage{isodate}
\usepackage{hyperref}
\usepackage{listings}
\usepackage{tabu}
\usepackage{todonotes}
\usepackage{xspace}

\lstset{
  language=C++,
  basicstyle=\ttfamily,
  tabsize=4,
  keywordstyle=\bfseries,
  commentstyle=\itshape,
  emph={implementation_defined,ImplementationDefined}, emphstyle=\itshape,
  emph={[2]\#include,\#define,\#ifdef,\#endif}, emphstyle={[2]\bfseries},
  extendedchars=true,
  escapeinside={/*@}{@*/},
  breaklines=true,
}

\def\cxx{\lstinline[language=C++]}
\def\sh{\lstinline[language=sh]}
\def\MPI{{\sffamily MPI}\xspace}

\begin{document}

\title{Thread-safety issues when using MPI}
\author{Jö Fahlke}

\maketitle

\section{\MPI Standards}

\subsection{\MPI 1.3}

The 1.3 standard of \MPI\cite{mpi-1.3} states (§2.6):
\begin{quote}
  \MPI does not specify the execution model for each process. %
  A process can be sequential, or can be multi-threaded, with threads possibly
  executing concurrently. %
  Care has been taken to make \MPI\ ''thread-safe,'' by avoiding the use of
  implicit state. %
  The desired interaction of \MPI with threads is that concurrent threads be
  all allowed to execute MPI calls, and calls be reentrant; a blocking MPI
  call blocks only the invoking thread, allowing the scheduling of another
  thread.
\end{quote}

(§3.4, \cxx{MPI_RSEND})
\begin{quote}
  In a multi-threaded implementation of \MPI, the system may de-schedule a
  thread that is blocked on a send or receive operation, and schedule another
  thread for execution in the same address space. %
  In such a case it is the user's responsibility not to access or modify a
  communication buffer until the communication completes. %
  Otherwise, the outcome of the computation is undefined.

  \begin{quote}
    {\em Rationale.} %
    We prohibit read accesses to a send buffer while it is being used, even
    though the send operation is not supposed to alter the content of this
    buffer. %
    This may seem more stringent than necessary, but the additional
    restriction causes little loss of functionality and allows better
    performance on some systems --- consider the case where data transfer is
    done by a DMA engine that is not cache-coherent with the main processor. %
    ({\em End of rationale.})
  \end{quote}

  \begin{quote}
    {\em Advice to implementors.} %
    \ldots

    In a multi-threaded environment, the execution of a blocking communication
    should block only the executing thread, allowing the thread scheduler to
    de-schedule this thread and schedule another thread for execution. %
    ({\em End of advice to implementors.})
  \end{quote}
\end{quote}

The following implies that \MPI operations may be posted by different threads
concurrently, at least under certain circumstances (§3.5 Semantics of
point-to-point communication):
\begin{quote}
  {\sffamily Fairness} %
  \MPI makes no guarantee of fairness in the handling of communication. %
  Suppose that a send is posted. %
  Then it is possible that the destination process repeatedly posts a receive
  that matches this send, yet the message is never received, because it is
  each time overtaken by another message, sent from another source. %
  Similarly, suppose that a receive was posted by a multi-threaded
  process. Then it is possible that messages that match this receive are
  repeatedly received, yet the receive is never satisfied, because it is
  overtaken by other receives posted at this node (by other executing
  threads). %
  It is the programmer's responsibility to prevent starvation in such
  situations.
\end{quote}

\subsection{\MPI 2.2}

See \cite{mpi-2.2}.

Thread compliance is not required, but if it is provided the standard says
{\em how} it must be provided (§12.1).

(§12.4 \MPI and Threads)
\begin{quote}
  The two main requirements for a thread-compliant implementation are listed
  below. %
  \begin{enumerate}
  \item All \MPI calls are thread-safe, i.\ e., two concurrently running
    threads may make \MPI calls and the outcome will be as if the calls
    executed in some order, even if their execution is interleaved.
  \item Blocking \MPI calls will block the calling thread only, allowing
    another thread to execute, if available. %
    The calling thread will be blocked until the event on which it is waiting
    occurs. %
    Once the blocked communication is enabled and can proceed, then the call
    will complete and the thread will be marked runnable, within a finite
    time.  %
    A blocked thread will not prevent progress of other runnable threads on
    the same process, and will not prevent them from executing \MPI calls.
  \end{enumerate}
\end{quote}

Other things to keep in mind:
\begin{itemize}
\item \cxx{MPI_FINELIZE} must be called in the same thread that called
  \cxx{MPI_INIT}.%
  (§12.4.2)
\end{itemize}

Four levels of threading support are defined.
\begin{description}
\newsavebox\dt
\savebox\dt{\cxx{MPI_THREAD_SINGLE}}
\item[\usebox\dt] The process may not use threads. %
  Equivalent to calling \cxx{MPI_INIT}. %
\savebox\dt{\cxx{MPI_THREAD_FUNNELED}}
\item[\usebox\dt] The process may use threads, but only the {\em main thread}
  may make \MPI calls. %
  The main thread is the thread that called \cxx{MPI_INIT_THREAD} (or
  \cxx{MPI_INIT}), and \cxx{MPI_FINALIZE} must always be called by the main
  thread, regardless of threading level. %
\savebox\dt{\cxx{MPI_THREAD_SERIALIZED}}
\item[\usebox\dt] The process may use threads, and multiple threads may issue
  \MPI calls, but they may not do so concurrently. %
\savebox\dt{\cxx{MPI_THREAD_MULTIPLE}}
\item[\usebox\dt] The process may use threads and multiple threads may issue
  \MPI calls concurrently.
\end{description}
These can be requested by using \cxx{MPI_INIT_THREAD} instead of
\cxx{MPI_INIT}. %
However, what you get is not necessarily what you requested;
\cxx{MPI_INIT_THREAD} will inform you of the level that is actually
provided. %
The level of provided thread support can also be queried with
\cxx{MPI_QUERY_THREAD}.

\subsection{\MPI 3.1}

Thread support in \MPI 3.1\cite{mpi-3.1} seems to be pretty identical to 2.2.

\section{Implementations}

\subsection{Voices from the Web}

\subsubsection{StackOverflow answer by Hristo Iliev (2013-02-12)}

After recounting\footnote{\url{http://stackoverflow.com/a/14837206/5165306},
  retrieved 2015-09-07}
how to properly initialize the library to request thread safety the poster
goes on to discuss threading support in actual implementations:

\begin{quote}
  \ldots

  I've already said that Open MPI has to be compiled with the proper flags
  enabled in order to support \cxx{MPI_THREAD_MULTIPLE}. %
  But there is another catch - it's InfiniBand component is not thread-safe
  and hence Open MPI would not use native InfiniBand communication when
  initialised at full thread support level.

  Intel's \MPI comes in two different flavours - one with and one without
  support for full multithreading. %
  Multithreaded support is enabled by passing the \sh{-mt_mpi} option to the
  \MPI compiler wrapper which enables linking with the MT version. %
  This option is also implied if OpenMP support or the autoparalleliser is
  enabled. %
  I am not aware how the InfiniBand driver in IMPI works when full thread
  support is enabled.

  MPICH(2) does not support InfiniBand, hence it is thread-safe and probably
  most recent versions provide \cxx{MPI_THREAD_MULTIPLE} support out of the
  box.

  MVAPICH is the basis on which Intel MPI is built and it supports
  InfiniBand. %
  I have no idea how it behaves at full thread support level when used on a
  machine with InfiniBand.

  The note about multithreaded InfiniBand support is important since lot of
  compute clusters nowadays use InfiniBand fabrics. %
  With the IB component (openib BTL in Open MPI) disabled, most MPI
  implementations switch to another protocol, for example TCP/IP (tcp BTL in
  Open MPI), which results in much slower and more latent communication
\end{quote}

\subsubsection{Asynchronous MPI for the Masses}

This is a paper\cite{ampi-2013} by Wellein {\em et.\ al.}\ in which they
introduce a transparent wrapper library to improve the performance of
asynchronous \MPI calls by progressing them in a {\em progress thread}. %
One of the preconditions for this to work is that the wrapped \MPI library has
support for \cxx{MPI_THREAD_MULTIPLE}, and they include a short overview of
the support in several libraries. %
I'll try to summarize the most important points:
\begin{description}
\item[Intel \MPI 4.0.3] Has support for \cxx{MPI_THREAD_MULTIPLE} and
  InfiniBand, but apparently there are bugs that lead to deadlocks.
\item[Open \MPI 1.6.3] The InfiniBand backend is not threadsafe and cannot be
  used together with \cxx{MPI_THREAD_MULTIPLE}. %
  Requesting \cxx{MPI_THREAD_MULTIPLE} leads to fallback to TCP.
\item[MPICH2] No support for InfiniBand, but support for
  \cxx{MPI_THREAD_MULTIPLE}.
\item[MVAPICH 1.9a2] Support for \cxx{MPI_THREAD_MULTIPLE}. %
  No word on support for InfiniBand.
\item[IBM \MPI 1.2] Support for \cxx{MPI_THREAD_MULTIPLE}. %
  No word on support for InfiniBand.
\item[Cray \MPI (standard configuration)] Support for
  \cxx{MPI_THREAD_MULTIPLE}. %
  No word on support for InfiniBand.
\end{description}
\subsection{Open \MPI}

The FAQ
states\footnote{\url{https://www.open-mpi.org/faq/?category=all\#thread-support}}:
\begin{quote}
  {\bf 18. Is Open MPI thread safe?}

  Support for \cxx{MPI_THREAD_MULTIPLE} (i.\ e.,\ multiple threads executing
  within the \MPI library) and asynchronous message passing progress (i.\ e.,\
  continuing message passing operations even while no user threads are in the
  \MPI library) has been designed into Open \MPI from its first planning
  meetings. %

  Support for \cxx{MPI_THREAD_MULTIPLE} is included in the first version of
  Open \MPI, but it is only lightly tested and likely still has some bugs. %
  Support for asynchronous progress is included in the TCP point-to-point
  device, but it, too, has only had light testing and likely still has bugs.

  Completing the testing for full support of \cxx{MPI_THREAD_MULTIPLE} and
  asynchronous progress is planned in the near future.
\end{quote}

That statement was retrieved 2015-09-04. %
It first appeared
2011-07-29\footnote{\url{https://web.archive.org/web/20110729154406/http://www.open-mpi.org/faq/?category=all\#thread-support}}.

It appears that to get support for \cxx{MPI_THREAD_MULTIPLE} one must run
\sh{configure} with
\sh{--enable-mpi-threads}\footnote{\url{http://www.nwchem-sw.org/index.php/Special:AWCforum/sp/id1976}}.

\subsection{MPICH}

The FAQ in the wiki
states\footnote{\url{https://wiki.mpich.org/mpich/index.php/Frequently_Asked_Questions\#Q:_Can_MPI_be_used_to_program_multicore_systems.3F}}:
\begin{quote}
  {\bf Q: Can MPI be used to program multicore systems?}

  A: There are two common ways to use MPI with multicore processors or multiprocessor nodes:

  \begin{enumerate}
  \item Use one MPI process per core (here, a core is defined as a program
    counter and some set of arithmetic, logic, and load/store units).
  \item Use one MPI process per node (here, a node is defined as a collection
    of cores that share a single address space). %
    Use threads or compiler-provided parallelism to exploit the multiple
    cores. %
    OpenMP may be used with \MPI; the loop-level parallelism of OpenMP may be
    used with any implementation of \MPI (you do not need an \MPI that
    supports \cxx{MPI_THREAD_MULTIPLE} when threads are used only for
    computational tasks). %
    This is sometimes called the hybrid programming model.
  \end{enumerate}

  MPICH automatically recognizes multicore architectures and optimizes
  communication for such platforms. %
  No special configure option is required.
\end{quote}

That statement was retrieved 2015-09-04. %
According to the wiki history, it appeared when the FAQ was moved to the wiki
''from MPICH2'' in 2009-03-25 (minus the last paragraph). %
I could not find out what is meant by ''MPICH2''.

\section{Conclusions}

It appears that full multithreading support is present in most \MPI
implementations. %
Notable exceptions are Intel \MPI (4.0.3), which is buggy, and OpenMPI
(1.6.3), which cannot use InfiniBand when full thread support is
enabled\cite{ampi-2013}. %
The current version of Intel \MPI is 5.1 and the current version of OpenMPI is
1.10.0, so there may well be improvements -- still, clusters tend to use older
software versions, so we may not want to rely on such improvements.

Therefore it seems best to target \cxx{MPI_THREAD_SERIALIZED}. %
This still allows to overlap communication and computation, and does not
require alterations to the current dune-grid communication interface. %
\todo{How to ensure that \cxx{MPI_THREAD_SERIALIZED} is actually widely
  supported?} %

In principle we may be able to work with \cxx{MPI_THREAD_FUNNELED}, but that
would require that the user always invokes communication from the main thread,
which inverts the common paradigm of a separate communication thread. %

\cxx{MPI_THREAD_SINGLE} is out of question, since it would completely prohibit
any threading. %
But it does not seem that there is any serious \MPI implementation around
nowadays that is restricted to this.


\bibliographystyle{plain}
\bibliography{viewthreadsafe}

\end{document}

%%% Local Variables:
%%% mode: latex
%%% mode: TeX-PDF
%%% mode: orgtbl
%%% TeX-master: t
%%% mode: flyspell
%%% ispell-local-dictionary: "american"
%%% End:
