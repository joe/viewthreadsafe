#!/bin/sh

set -ex

for extra_libs in "" -pthread; do
    mpic++ -std=c++11 $extra_libs -Wall -O3 -o thread-support thread-support.cc

    for mpirun in "" "mpirun -np 2"; do
        for required in SINGLE FUNNELED SERIALIZED MULTIPLE; do
            env MPI_THREAD_REQUIRED="$required" $mpirun ./thread-support
        done
    done

done
