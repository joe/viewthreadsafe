#include <cstdlib>
#include <iostream>
#include <ostream>
#include <stdexcept>
#include <string>
#include <vector>

#include "mpi.h"

int parse_MPI_THREAD(const std::string &spec)
{
  if(spec == "SINGLE")     return MPI_THREAD_SINGLE;
  if(spec == "FUNNELED")   return MPI_THREAD_FUNNELED;
  if(spec == "SERIALIZED") return MPI_THREAD_SERIALIZED;
  if(spec == "MULTIPLE")   return MPI_THREAD_MULTIPLE;
  throw std::runtime_error(("Invalid thread spec "+spec).c_str());
}

std::string format_MPI_THREAD(int spec)
{
  using std::to_string;
  switch(spec) {
  case MPI_THREAD_SINGLE:     return "SINGLE";
  case MPI_THREAD_FUNNELED:   return "FUNNELED";
  case MPI_THREAD_SERIALIZED: return "SERIALIZED";
  case MPI_THREAD_MULTIPLE:   return "MULTIPLE";
  default:
    throw std::runtime_error("Invalid thread spec num " + to_string(spec));
  }
}

int main(int argc, char **argv)
{
  int required = parse_MPI_THREAD(std::getenv("MPI_THREAD_REQUIRED"));
  int provided;
  MPI_Init_thread(&argc, &argv, required, &provided);
  int size, rank;
  MPI_Comm_size(MPI_COMM_WORLD, &size);
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);
  std::vector<int> recvbuf(rank == 0 ? size : 0);
  MPI_Gather(&provided, 1, MPI_INT, recvbuf.data(), 1, MPI_INT, 0,
             MPI_COMM_WORLD);
  MPI_Finalize();
  if(rank == 0)
  {
    std::cout << "MPI version (static): " << MPI_VERSION << "."
              << MPI_SUBVERSION << std::endl;

    int version, subversion;
    MPI_Get_version(&version, &subversion);
    std::cout << "MPI version (dynamic): " << version << "." << subversion
              << std::endl;

#if MPI_VERSION >= 3
    char libversion[MPI_MAX_LIBRARY_VERSION_STRING];
    int libversionlen;
    MPI_Get_library_version(libversion, &libversionlen);
    std::cout << "Library version: " << libversion << std::endl;
#endif

    std::cout << "Required: " << format_MPI_THREAD(required) << std::endl
              << "Provided:" << std::endl;
    for(int i = 0; i < size; ++i)
      std::cout << "  Rank " << i << ": " << format_MPI_THREAD(recvbuf[i])
                << std::endl;
  }
}
